﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyHocSinhCap3.View
{
    /// <summary>
    /// Interaction logic for ThemSuaHocSinh.xaml
    /// </summary>
    public partial class ThemSuaHocSinh : Window
    {
        public delegate void Insert(HOCSINH hocsinh);
        public event Insert OnInsert = null;

        public delegate void Update(HOCSINH hocsinh);
        public event Update OnUpdate = null;

        HOCSINH hocsinhupdate = new HOCSINH();
        public ThemSuaHocSinh()
        {
            InitializeComponent();
            LoadData();
        }
        public ThemSuaHocSinh(HOCSINH hocsinh)
        {
            InitializeComponent();
            ListLop.IsEditable = true;
            LopDAO lopDAO = new LopDAO();
            ListLop.ItemsSource = lopDAO.ReadAll();
            txt_ID.Text = hocsinh.IDHS;
            txt_Ten.Text = hocsinh.TENHS;
            ListLop.Text = hocsinh.LOP.TENLOP;
            if (hocsinh.GIOITINH == "Nam") chbNam.IsChecked = true;
            else chbNu.IsChecked = true;
            dpNgaySinh.Text = hocsinh.NGAYSINH.ToString();
            txt_Email.Text = hocsinh.EMAIL;
            txt_DiaChi.Text = hocsinh.DIACHI;
            btnThem.Content = "Cập nhật";
            hocsinhupdate = hocsinh;
        }
        string IDLop;

        private void BtnThem_Click(object sender, RoutedEventArgs e)
        {
            HOCSINH hocsinh = GetHocSinhInfo();
            if (OnUpdate != null)
            {
                OnUpdate(hocsinh);
            }
            if (OnInsert != null)
            {
                OnInsert(hocsinh);
            }
            this.Close();
        }
        private HOCSINH GetHocSinhInfo()
        {
            HOCSINH hocsinh = new HOCSINH();
            hocsinh.IDHS = txt_ID.Text;
            hocsinh.TENHS = txt_Ten.Text;
            if (OnUpdate != null)
            {
                if (IDLop == null)
                {
                    hocsinh.IDLOP = hocsinhupdate.IDLOP;
                }
                else
                {
                    hocsinh.IDLOP = IDLop;
                }
            }
            if (OnInsert != null)
            {
                hocsinh.IDLOP = IDLop;
            }

            if (chbNam.IsChecked == true) hocsinh.GIOITINH = "Nam";
            if (chbNu.IsChecked == true) hocsinh.GIOITINH = "Nữ";
            if (chbNam.IsChecked == false && chbNu.IsChecked == false) hocsinh.GIOITINH = "Chưa có";
            hocsinh.NGAYSINH = DateTime.Parse(dpNgaySinh.Text);
            hocsinh.EMAIL = txt_Email.Text;
            hocsinh.DIACHI = txt_DiaChi.Text;
            HocSinhDAO hocsinhDAO = new HocSinhDAO();
            return hocsinh;
        }
        private void LoadData()
        {
            LopDAO lopDAO = new LopDAO();
            ListLop.ItemsSource = lopDAO.ReadAll();
        }

        private void ListLop_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListLop.IsEditable = false;
            ListLop.IsReadOnly = false;
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            IDLop = ((QuanLyHocSinhCap3.DTO.LopDTO)item).ID.ToString();
        }

        private void ListKhoi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            var ten = ((QuanLyHocSinhCap3.DTO.KhoiDTO)item).Ten.ToString();
            LopDAO lopDAO = new LopDAO();
            ListLop.ItemsSource = lopDAO.ReadByTenKhoi(ten);
        }

        private void ChbNam_Checked(object sender, RoutedEventArgs e)
        {
            if (chbNam.IsChecked == true)
            {
                chbNu.IsChecked = false;
            }
            else
            {
                chbNu.IsChecked = true;
            }
        }

        private void ChbNu_Checked(object sender, RoutedEventArgs e)
        {
            if (chbNu.IsChecked == true)
            {
                chbNam.IsChecked = false;
            }
            else
            {
                chbNam.IsChecked = true;
            }
        }
    }
}