﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyHocSinhCap3.UI
{
    /// <summary>
    /// Interaction logic for SuaDiem.xaml
    /// </summary>
    public partial class SuaDiem : Window
    {
        public delegate void Update(DIEM diem);
        public event Update OnUpdate = null;
        public SuaDiem()
        {
            InitializeComponent();
        }
        DiemDTO diemselection = new DiemDTO();
        DIEM tmpDIEM = new DIEM();
        public SuaDiem(DIEM diem)
        {
            InitializeComponent();
            tmpDIEM = diem;
            try
            {
                tb15p.Text = diem.DIEM15P;
                tb1tiet.Text = diem.DIEM1TIET;
                tbthi.Text = diem.DIEMHOCKY;
            }
            catch
            {
                MessageBox.Show("Lỗi load dữ liệu điểm lên form", "Error", MessageBoxButton.OK);
            }
        }
        private void BtnLuu_Click(object sender, RoutedEventArgs e)
        {
            DIEM diem = GetDiemInfo();
            if (OnUpdate != null)
            {
                OnUpdate(diem);
            }
            this.Close();

        }
        private void BtnHuy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private DIEM GetDiemInfo()
        {
            tmpDIEM.DIEM15P = tb15p.Text;
            tmpDIEM.DIEM1TIET = tb1tiet.Text;
            tmpDIEM.DIEMHOCKY = tbthi.Text;
            return tmpDIEM;
        }
    }
}
