﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class BangDiemTongDTO
    {
        public HocSinhDTO HocSinh { get; set; }
        public float DiemPhayHK1 { get; set; }
        public float DiemPhayHK2 { get; set; }
    }
}
