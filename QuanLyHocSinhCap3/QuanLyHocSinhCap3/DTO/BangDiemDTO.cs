﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class BangDiemDTO
    {
        public int ID { get; set; }
        public HocKyDTO HocKy { get; set; }
        public HocSinhDTO HocSinh { get; set; }
        public DiemDTO MonToanHoc { get; set; }
        public DiemDTO MonLyHoc { get; set; }
        public DiemDTO MonHoaHoc { get; set; }
        public DiemDTO MonSinhHoc { get; set; }
        public DiemDTO MonLichSu { get; set; }
        public DiemDTO MonDiaLy { get; set; }
        public DiemDTO MonNguVan { get; set; }
        public DiemDTO MonDaoDuc { get; set; }
        public DiemDTO MonTheDuc { get; set; }
    }
}
