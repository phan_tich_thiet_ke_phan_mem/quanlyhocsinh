﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class BaoCaoDTO
    {
        public int STT { get; set; }
        public LopDTO Lop { get; set; }
        public int SoLuongDat { get; set; }
        public int TyLe { get; set; }
    }
}
