﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class TaiKhoanDTO
    {
        public String TenDangNhap { get; set; }
        public String MatKhau { get; set; }
        public String Ten { get; set; }
        public String GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public LoaiTaiKhoanDTO LoaiTaiKhoan { get; set; }
    }
}
