﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class LopDTO
    {
        public String ID { get; set; }
        public String Ten { get; set; }
        public int SiSo { get; set; }
        public int SiSoMax { get; set; }
        public KhoiDTO Khoi { get; set; }
        public GiaoVienDTO GiaoVien { get; set; }
    }
}
