﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class MonHocDTO
    {
        public String ID { get; set; }
        public String Ten { get; set; }
    }
}
