﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class HocKyDTO
    {
        public String ID { get; set; }
        public string Ten { get; set; }
    }
}
