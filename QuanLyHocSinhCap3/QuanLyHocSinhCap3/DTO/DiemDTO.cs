﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DTO
{
    public class DiemDTO
    {
        public HocSinhDTO HocSinh { get; set; }
        public HocKyDTO HocKy { get; set; }
        public MonHocDTO MonHoc { get; set; }
        public String Diem15P { get; set; }
        public String Diem1Tiet { get; set; }
        public String DiemHocKy { get; set; }
        public String DiemTongKet { get; set; }
    }
}
