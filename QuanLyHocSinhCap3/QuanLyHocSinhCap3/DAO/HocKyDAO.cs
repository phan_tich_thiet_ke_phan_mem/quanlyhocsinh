﻿using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class HocKyDAO
    {
        public List<HocKyDTO> ReadAll()
        {
            var listDB = new List<HocKyDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from hocky in db.HOCKies select hocky;
                foreach (var item in table)
                {
                    var tmp = new HocKyDTO();
                    tmp.ID = item.IDHK;
                    tmp.Ten = item.TENHK;
                    listDB.Add(tmp);
                }
            }
            return listDB;
        }
    }
}
