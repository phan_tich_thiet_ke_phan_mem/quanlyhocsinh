﻿using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class BangDiemTongDAO
    {
        public List<BangDiemTongDTO> ReadAll()
        {
            List<BangDiemTongDTO> ListBangDiemTong = new List<BangDiemTongDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {   
                var lisths = from hocsinh in db.HOCSINHs select hocsinh;
                foreach (var hs in lisths)
                {
                    var bangdiemtong = new BangDiemTongDTO();
                    bangdiemtong.HocSinh = new HocSinhDTO();
                    bangdiemtong.HocSinh.Lop = new LopDTO();
                    bangdiemtong.DiemPhayHK1 = 0;
                    bangdiemtong.DiemPhayHK2 = 0;

                    bangdiemtong.HocSinh.ID = hs.IDHS;
                    bangdiemtong.HocSinh.Ten = hs.TENHS;
                    bangdiemtong.HocSinh.Lop.Ten = hs.LOP.TENLOP;
                    var listd = (from d in db.DIEMs where (hs.IDHS == d.IDHS) select d).ToList();
                    var hesohk1 = 0;
                    var hesohk2 = 0;
                    foreach (var diem in listd)
                    {    
                        if (diem.IDHK == "001")
                        {
                            hesohk1++;
                            bangdiemtong.DiemPhayHK1 = bangdiemtong.DiemPhayHK1 + float.Parse(new DiemDAO().TongPhay(diem));
                        }

                        if (diem.IDHK == "002")
                        {
                            hesohk2++;
                            bangdiemtong.DiemPhayHK2 = bangdiemtong.DiemPhayHK2 + float.Parse(new DiemDAO().TongPhay(diem));
                        }
                        bangdiemtong.DiemPhayHK1 = bangdiemtong.DiemPhayHK1 / hesohk1;
                        bangdiemtong.DiemPhayHK2 = bangdiemtong.DiemPhayHK2 / hesohk2;
                    }
                    ListBangDiemTong.Add(bangdiemtong);
                }
                return ListBangDiemTong;
            }
        }
    }
}
