﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class QuyDinhDAO
    {
        public void UpdateDiemChuan( float diem)
        {
            using(var db = new ProcessData().ConnectionDB())
            {
                var quydinh = (from qd in db.QUYDINHs where (qd.IDQUYDINH=="QUYDINH") select qd).Single();
                quydinh.DIEMCHUANDANHGIA = diem;
                db.SaveChanges();
            }
        }
        public void UpdateTuoiGioiHan(int min, int max)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var quydinh = (from qd in db.QUYDINHs where (qd.IDQUYDINH == "QUYDINH") select qd).Single();
                quydinh.TUOITOITHIEU = min;
                quydinh.TUOITOIDA = max;
                db.SaveChanges();
            }
        }
    }

}
