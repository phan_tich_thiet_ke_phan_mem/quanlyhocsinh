﻿using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class KhoiDAO
    {
        public List<KhoiDTO> ReadAll()
        {
            var listDB_Khoi = new List<KhoiDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from khoi in db.KHOIs select khoi;
                foreach (var item in table)
                {
                    var tmp = new KhoiDTO();
                    tmp.ID = item.IDKHOI;
                    tmp.Ten = item.TENKHOI;
                    listDB_Khoi.Add(tmp);
                }
            }
            return listDB_Khoi;
        }
    }
}
