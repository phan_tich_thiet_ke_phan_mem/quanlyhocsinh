﻿using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class BaoCaoMonHocDAO
    {
        public List<BaoCaoDTO> ReadByIDHK_IDMH(String IDHK, String IDMH)
        {
            List<BaoCaoDTO> listDB = new List<BaoCaoDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var quydinh = (from qd in db.QUYDINHs where (qd.IDQUYDINH == "QUYDINH") select qd).Single();
                var tmp = (from d in db.DIEMs
                       where (d.HOCKY.IDHK == IDHK && d.MONHOC.IDMH == IDMH)
                       select d).ToList();//lọc danh trong danh điểm thõa mãn IDHK và IDMonHoc =>danh sách

                var danhsachlop = (from lop in db.LOPs select lop).ToList();
                int i = 0;
                foreach(var itemlop in danhsachlop)
                {
                    i++;
                    var tmpitem = new BaoCaoDTO();
                    tmpitem.STT = i;
                    tmpitem.Lop = new LopDTO();

                    tmpitem.Lop.ID = itemlop.IDLOP;
                    tmpitem.Lop.Ten = itemlop.TENLOP;
                    tmpitem.Lop.SiSo = itemlop.SISO.Value;
                    foreach (var itemdiem in tmp)
                    {      
                        if (itemlop.IDLOP == itemdiem.HOCSINH.IDLOP)
                        {        
                            if(float.Parse(new DiemDAO().TongPhay(itemdiem)) > quydinh.DIEMCHUANDANHGIA )
                            {
                                tmpitem.SoLuongDat++;
                            }
                        }
                    }
                    tmpitem.TyLe = tmpitem.SoLuongDat * 100 / itemlop.SISO.Value ;
                    listDB.Add(tmpitem);
                }
            }
            return listDB;
        }
        public List<BaoCaoDTO> ReadByIDHK(String IDHK)
        {
            List<BaoCaoDTO> listDB = new List<BaoCaoDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var quydinh = (from qd in db.QUYDINHs where (qd.IDQUYDINH == "QUYDINH") select qd).Single();
                var tmp = (from d in db.DIEMs
                           where (d.HOCKY.IDHK == IDHK)
                           select d).ToList();//lọc danh trong danh điểm thõa mãn IDHK và IDMonHoc =>danh sách

                var danhsachlop = (from lop in db.LOPs select lop).ToList();
                int i = 0;
                foreach (var itemlop in danhsachlop)
                {

                    i++;
                    var tmpitem = new BaoCaoDTO();
                    tmpitem.STT = i;
                    tmpitem.Lop = new LopDTO();

                    tmpitem.Lop.ID = itemlop.IDLOP;
                    tmpitem.Lop.Ten = itemlop.TENLOP;
                    tmpitem.Lop.SiSo = itemlop.SISO.Value;

                    var listhocsinh = new HocSinhDAO().ReadByIDLop(itemlop.IDLOP);
                    foreach(var hocsinh in listhocsinh)
                    {
                        bool dat = false;
                        foreach (var itemdiem in tmp)
                        {
                            if (itemdiem.IDHS == hocsinh.ID)
                            {
                                if (float.Parse(new DiemDAO().TongPhay(itemdiem)) > quydinh.DIEMCHUANDANHGIA)
                                {
                                    dat = true;
                                }
                            }
                            else
                            {
                                dat = false;
                                break;
                            }
                        }
                        if(dat == true)
                        {
                            tmpitem.SoLuongDat++;
                        }
                    }
                    tmpitem.TyLe = tmpitem.SoLuongDat * 100 / itemlop.SISO.Value;
                    listDB.Add(tmpitem);
                }
            }
            return listDB;
        }
    }
}
