﻿using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class LopDAO
    {
        public List<LopDTO> ReadAll()
        {
            var listDB_Lop = new List<LopDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from lop in db.LOPs select lop;
                foreach (var item in table)
                {
                    var tmp = new LopDTO();
                    tmp.Khoi = new KhoiDTO();
                    tmp.GiaoVien = new GiaoVienDTO();
                    tmp.ID = item.IDLOP;
                    tmp.Ten = item.TENLOP;
                    tmp.SiSo = item.SISO.Value;
                    tmp.SiSoMax = item.SISOMAX.Value;
                    tmp.GiaoVien.ID = item.IDGV;
                    tmp.GiaoVien.Ten = item.GIAOVIEN.TENGV;
                    tmp.Khoi.ID = item.IDKHOI;
                    tmp.Khoi.Ten = item.KHOI.TENKHOI;
                    listDB_Lop.Add(tmp);
                }
            }
            return listDB_Lop;
        }
        public LopDTO ReadByTenLop(String tenlop)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var tmp = new LopDTO();
                var lop = (from item in db.LOPs where item.TENLOP == tenlop select item).Single();
                tmp.GiaoVien = new GiaoVienDTO();
                tmp.Ten = lop.TENLOP;
                tmp.SiSo = lop.SISO.Value;
                tmp.GiaoVien.Ten = lop.GIAOVIEN.TENGV;
                return tmp;
            }
        }
        public List<LopDTO> ReadByTenKhoi(String TenKhoi)
        {
            var listDB_Lop = new List<LopDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from lop in db.LOPs select lop;
                foreach (var item in table)
                {
                    if (item.KHOI.TENKHOI == TenKhoi)
                    {
                        var tmp = new LopDTO();
                        tmp.Khoi = new KhoiDTO();
                        tmp.GiaoVien = new GiaoVienDTO();
                        tmp.ID = item.IDLOP;
                        tmp.Ten = item.TENLOP;
                        tmp.GiaoVien.ID = item.IDGV;
                        tmp.GiaoVien.Ten = item.GIAOVIEN.TENGV;
                        tmp.Khoi.ID = item.IDKHOI;
                        tmp.Khoi.Ten = item.KHOI.TENKHOI;
                        listDB_Lop.Add(tmp);
                    }
                    else { }
                }
            }
            return listDB_Lop;
        }
        public void UpdateSiSo(string ID, int siso)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var lopupdate = db.LOPs.Find(ID);
                // hocsinhupdate.IDHS = hocsinh.IDHS;
                lopupdate.SISO = siso;
                db.SaveChanges();
            }
        }
        public Boolean KiemTraSiSo(String IDLop)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var lop = (from item in db.LOPs where item.IDLOP == IDLop select item).Single();
                if (lop.SISO < lop.SISOMAX)
                    return true;
            }
                return false;
        }
        public void Update( LopDTO lophoc)
        {
            using(var db = new ProcessData().ConnectionDB())
            {
                var lop = (from l in db.LOPs where l.IDLOP == lophoc.ID select l).Single();
                lop.TENLOP = lophoc.Ten;
                lop.SISOMAX = lophoc.SiSoMax;
                db.SaveChanges();
            }
        }
    }
}
