﻿using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class MonHocDAO
    {
        public List<MonHocDTO> ReadAll()
        {
            var listDB = new List<MonHocDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from monhoc in db.MONHOCs select monhoc;
                foreach (var item in table)
                {
                    var tmp = new MonHocDTO();
                    tmp.ID = item.IDMH;
                    tmp.Ten = item.TENMH;
                    listDB.Add(tmp);
                }
            }
            return listDB;
        }
        public void Update(MonHocDTO mon)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var monhoc = (from d in db.MONHOCs where d.IDMH == mon.ID select d).Single();
                monhoc.TENMH = mon.Ten;
                db.SaveChanges();
            }
        }
    }
}
