﻿using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class TaiKhoanDAO
    {
        public bool DangNhap(String TenDangNhap, String MatKhau)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                try
                {
                      var item = (from tk in db.TAIKHOANs where tk.TENTAIKHOAN == TenDangNhap select tk).Single();
                    if (item == null)
                    {
                        return false;
                    }
                    if (item.MATKHAU == MatKhau)
                    {
                        return true;
                    }
                }
                catch { }
            }
            return false;
        }
        public void UpdatePassWord(TaiKhoanDTO taikhoan , String pass)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var item = (from tk in db.TAIKHOANs where tk.TENTAIKHOAN == taikhoan.TenDangNhap select tk).Single();
                item.MATKHAU = pass;
                db.SaveChanges();
            }
        }
        public void Update(String TenTaiKhoan, TaiKhoanDTO taikhoan)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var item = (from tk in db.TAIKHOANs where tk.TENTAIKHOAN == TenTaiKhoan select tk).Single();
                item.TEN = taikhoan.Ten;
                item.NGAYSINH = taikhoan.NgaySinh;
                item.GIOITINH = taikhoan.GioiTinh;
                //item.LOAITAIKHOAN.TENLOAITK = taikhoan.LOAITAIKHOAN.TENLOAITK;
                db.SaveChanges();
            }
        }
        public TaiKhoanDTO Read(String TenDangNhap, String MatKhau)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                try
                {
                    var item = (from tk in db.TAIKHOANs where tk.TENTAIKHOAN == TenDangNhap select tk).Single();
                    TaiKhoanDTO tmp = new TaiKhoanDTO();
                    tmp.LoaiTaiKhoan = new LoaiTaiKhoanDTO();
                    tmp.TenDangNhap = item.TENTAIKHOAN;
                    tmp.MatKhau = item.MATKHAU;
                    tmp.Ten = item.TEN;
                    tmp.NgaySinh = item.NGAYSINH.Value;
                    tmp.GioiTinh = item.GIOITINH;
                    tmp.LoaiTaiKhoan.ID = item.IDLOAITK;
                    tmp.LoaiTaiKhoan.Ten = item.LOAITAIKHOAN.TENLOAITK;
                    return tmp;
                }
                catch { }
            }
            return null;
        }
    }
}
