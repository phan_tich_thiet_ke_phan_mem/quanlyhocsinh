﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class HocKyBUS
    {
        public List<HocKyDTO> ReadAll()
        {
            return new HocKyDAO().ReadAll();
        }
    }
}
