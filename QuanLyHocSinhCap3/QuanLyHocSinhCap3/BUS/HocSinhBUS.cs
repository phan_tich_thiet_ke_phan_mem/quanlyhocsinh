﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class HocSinhBUS
    {
        public List<HocSinhDTO> ReadAll()
        {
            return new HocSinhDAO().ReadAll();
        }
        public List<HocSinhDTO> ReadByIDHocSinh(string ID)
        {
            return new HocSinhDAO().ReadByIDHocSinh(ID);
        }
        public void Insert(HOCSINH hocsinh)
        {
            new HocSinhDAO().Insert(hocsinh);
        }
        public void Delete(HocSinhDTO hocsinh)
        {
            new HocSinhDAO().Delete(hocsinh);
        }
        public void Update(string ID, HOCSINH hocsinh)
        {
            new HocSinhDAO().Update(ID, hocsinh);
        }
        public List<HocSinhDTO> ReadByIDLop(string IDLop)
        {     
            return new HocSinhDAO().ReadByIDLop(IDLop);
        }
        public List<HocSinhDTO> ReadByTenLop(string TenLop)
        {          
            return new HocSinhDAO().ReadByTenLop(TenLop);
        }
    }
}
