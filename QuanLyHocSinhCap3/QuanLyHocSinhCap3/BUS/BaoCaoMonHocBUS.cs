﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class BaoCaoMonHocBUS
    {
        public List<BaoCaoDTO> ReadByIDHK_IDMH(String IDHK, String IDMH)
        {
            return new BaoCaoMonHocDAO().ReadByIDHK_IDMH(IDHK, IDMH);
        }
        public List<BaoCaoDTO> ReadByIDHK(String IDHK)
        {
            return new BaoCaoMonHocDAO().ReadByIDHK(IDHK);
        }
    }
}
