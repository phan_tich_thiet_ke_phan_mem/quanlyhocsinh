﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class TaiKhoanBUS
    {
        public bool DangNhap(String TenDangNhap, String MatKhau)
        {
            return new TaiKhoanDAO().DangNhap(TenDangNhap, MatKhau);
        }
        public void UpdatePassWord(TaiKhoanDTO taikhoan, String pass)
        {
            new TaiKhoanDAO().UpdatePassWord(taikhoan, pass);
        }
        public void Update(String TenTaiKhoan, TaiKhoanDTO taikhoan)
        {
            new TaiKhoanDAO().Update(TenTaiKhoan, taikhoan);
        }
        public TaiKhoanDTO Read(String TenDangNhap, String MatKhau)
        {
            return new TaiKhoanDAO().Read(TenDangNhap, MatKhau);
        }
    }
}
