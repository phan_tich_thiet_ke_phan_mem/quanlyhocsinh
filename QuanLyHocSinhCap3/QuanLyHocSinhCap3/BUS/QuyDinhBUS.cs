﻿using QuanLyHocSinhCap3.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class QuyDinhBUS
    {
        public void UpdateDiemChuan(float diem)
        {
            new QuyDinhDAO().UpdateDiemChuan(diem);
        }
        public void UpdateTuoiGioiHan(int min, int max)
        {
            new QuyDinhDAO().UpdateTuoiGioiHan(min, max);
        }
    }

}
