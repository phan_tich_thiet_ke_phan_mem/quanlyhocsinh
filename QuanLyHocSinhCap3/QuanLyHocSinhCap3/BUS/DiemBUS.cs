﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuanLyHocSinhCap3.BUS
{
    public class DiemBUS
    {
        public List<DiemDTO> ReadAll()
        {
            return new DiemDAO().ReadAll();
        }
        public List<DiemDTO> ReadByIDHocSinh(String IDHOCSINH)
        {
            return new DiemDAO().ReadByIDHocSinh(IDHOCSINH);
        }
        public List<DiemDTO> ReadByIDMonHoc(String IDMonHoc)
        {
            return new DiemDAO().ReadByIDMonHoc(IDMonHoc);
        }
        public List<DiemDTO> ReadByIDHocKy(String IDHocKy)
        {
            return new DiemDAO().ReadByIDHocKy(IDHocKy);
        }
        public List<DiemDTO> ReadDiemByIDHS_IDMH_IDHK_IDLOP(String IDHS, String IDMH, String IDHK, String IDLOP)
        {
            return ReadDiemByIDHS_IDMH_IDHK_IDLOP(IDHS, IDMH, IDHK, IDLOP);
        }
        public void Create()
        {
            new DiemDAO().Create();
        }
        public void Insert(DIEM diem)
        {
            new DiemDAO().Insert(diem);
        }
        public void Update(string ID, string IDHK, string IDMH, DIEM diem)
        {
            new DiemDAO().Update(ID, IDHK, IDMH, diem);
        }
        public void Delete(DIEM diem)
        {
            new DiemDAO().Delete(diem);
        }
        public string TongPhay(DIEM diem)
        {
            return new DiemDAO().TongPhay(diem);
        }
    }
}
