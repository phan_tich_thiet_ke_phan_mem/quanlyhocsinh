﻿using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.BUS
{
    public class LopBUS
    {
        public List<LopDTO> ReadAll()
        {
            return new LopDAO().ReadAll();
        }
        public LopDTO ReadByTenLop(String tenlop)
        {
            return new LopDAO().ReadByTenLop(tenlop);
        }
        public List<LopDTO> ReadByTenKhoi(String TenKhoi)
        {
            return new LopDAO().ReadByTenKhoi(TenKhoi);
        }
        public void UpdateSiSo(string ID, int siso)
        {
            new LopDAO().UpdateSiSo(ID, siso);
        }
        public bool KiemTraSiSo(String IDLop)
        {
            return new LopDAO().KiemTraSiSo(IDLop);
        }
        public void Update(LopDTO lophoc)
        {
            new LopDAO().Update(lophoc);
        }
    }
}
